package com.sample.crud.demo.controller;


import com.sample.crud.demo.entity.Person;
import com.sample.crud.demo.service.PersonService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping(value = {"", "/"})
    public List<Person> findAll() {
        return personService.findAll();
    }

    @GetMapping(value = {"/{id}"})
    public Person findById(@PathVariable(name = "id") String id) {
        return personService.findById(id);
    }

    @GetMapping(value = {"/find"})
    public List<Person> findByName(@RequestParam(name = "name") String name) {
        return personService.findByName(name);
    }


    @PostMapping(value = {"", "/"})
    public Person save(@RequestBody Person person) {
        return personService.save(person);
    }

    @PutMapping(value = {"", "/"})
    public Person update(@RequestBody Person person) {
        return personService.update(person);
    }

    @DeleteMapping(value = {"/{id}"})
    public Boolean deleteById(@PathVariable(name = "id") String id) {
        return personService.deleteById(id);
    }
}
