package com.sample.crud.demo.service;

import com.sample.crud.demo.entity.Person;

import java.util.List;

public interface PersonService {

    Person findById(String id);

    List<Person> findAll();

    List<Person> findByName(String name);

    Person save(Person person);

    Person update(Person person);

    Boolean deleteById(String id);
}
