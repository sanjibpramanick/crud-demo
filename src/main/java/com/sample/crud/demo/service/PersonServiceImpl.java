package com.sample.crud.demo.service;

import com.sample.crud.demo.entity.Person;
import com.sample.crud.demo.repository.PersonRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Person findById(String id) {
        return personRepository.findById(id).orElse(null);
    }

    @Override
    public List<Person> findAll() {
        return personRepository.findAll();
    }

    @Override
    public List<Person> findByName(String name) {
        return personRepository.findAllByNameContains(name);
    }

    @Override
    public Person save(Person person) {
        return personRepository.save(person);
    }

    @Override
    public Person update(Person person) {
        return personRepository.save(person);
    }

    @Override
    public Boolean deleteById(String id) {
        try {
            personRepository.deleteById(id);
            return Boolean.TRUE;
        } catch (Exception ex) {
            log.error("DB Record could not be deleted: {}", id);
            return Boolean.FALSE;
        }

    }
}
