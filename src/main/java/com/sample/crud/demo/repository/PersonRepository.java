package com.sample.crud.demo.repository;

import com.sample.crud.demo.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonRepository extends JpaRepository<Person,String> {

    List<Person> findAllByNameContains(String name);
}
